# Mutsa Individual Project
### [➤ Swagger Specification](http://ec2-13-125-18-20.ap-northeast-2.compute.amazonaws.com:8080/swagger-ui/)

## Project Summary
로그인, 글쓰기, 수정, 삭제, 피드기능(페이징), 댓글, 좋아요, 알림 기능을 구현한 **SNS 웹 애플리케이션**

## Tech Stack
- 에디터 : Intellij Educational
- 개발 툴 : SpringBoot 2.7.5
- 자바 : JAVA 11
- 빌드 : Gradle 6.8
- 서버 : AWS EC2
- 배포 : Docker
- 데이터베이스 : MySql 8.0
- 필수 라이브러리 : SpringBoot Web, MySQL, Spring Data JPA, Lombok, Spring Security

## Endpoint
| API 종류  |  HTTP  |         URI         |            API 설명             |
|:-------:|:------:|:-------------------:|:-----------------------------:|
| `hello` |  GET   |    /api/v1/hello    | API test (return String data) |
| `users` |  POST  | /api/v1/users/join  |             회원가입              |
| `users` |  POST  | /api/v1/users/login |              로그인              |
| `posts` |  POST  |    /api/v1/posts    |            게시글 작성             |
| `posts` |  GET   | /api/v1/posts/{id}  |            게시글 수정             |
| `posts` | DELETE | /api/v1/posts/{id}  |            게시글 삭제             |
| `posts` |  GET   | /api/v1/posts/{id}  |           단일 게시글 조회           |
| `posts` |  GET   |   /api/v1/posts/    |          게시글 리스트 조회           |

## Feature Progress
### Settings
- [x] AWS EC2에 Docker로 배포
- [x] gitlab 배포파일 & ec2 크론탭 설정
- [x] Swagger 3.0 추가

### Development
- [x] 회원가입
- [x] 로그인
- [x] 게시글 CRUD
  - [x] 작성(Create)
  - [x] 단일 조회(Read)
  - [x] 리스트 조회(Read)
  - [x] 수정(Update)
  - [x] 삭제(Delete)
- [x] 좋아요, 취소

### Test
- [ ] 회원가입
- [ ] 로그인
- [ ] 게시글 CRUD
    - [ ] 작성(Create)
    - [ ] 단일 조회(Read)
    - [ ] 리스트 조회(Read)
    - [ ] 수정(Update)
    - [ ] 삭제(Delete)
- [ ] 좋아요, 취소

## ERD
![erd](https://imgur.com/UzdwP7I){: width="40%" height="40%"}

## Retrospective
### Liked
* 여러 소스의 레퍼런스 참고(공식 문서, Stack Overflow etc)
* 클린 코드를 위한 잦은 리팩토링
* ReadMe 문서 가독성 향상
* 세세한 커밋 단위(초반 한정)
### Learned
* Gitlab CI/CD (with Docker)
* 회원가입 및 로그인 REST API 작성 방법
* Response 와 Request 설계
* CRUD 기능 구현 방법
* 페이징 기능 구현 방법
### Lacked
* 단위 테스트
* 통합 테스트
* 미션 2 요구사항
* JPA 동작 원리
* 꾸준한 커밋
* 데드라인 맞추기
### Longed for
* 테스트 기반 개발(TDD)를 위한 배경지식 공부
* 미션 2의 작은 기능부터 도입하며 투트랙으로 개발
* 틈틈히 JPA 및 Spring 애너테이션 공부
* 커밋 주기를 30분으로 설정
* 개발 시간 확보 및 관리