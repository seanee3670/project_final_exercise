package com.project_final.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// Annotation 없이 POJO 로 로직 검증
class AlgorithmServiceTest {

    // Spring 없이 테스트를 하기 위해 new 로 초기화
    AlgorithmService algorithmService = new AlgorithmService();

    @Test
    @DisplayName("자릿수의 합 로직 검증")
    void sumOfDigits() {
        assertEquals(21, algorithmService.sumOfDigits(687));
        assertEquals(22, algorithmService.sumOfDigits(787));
        assertEquals(0, algorithmService.sumOfDigits(0));
        assertEquals(5, algorithmService.sumOfDigits(11111));
    }
}