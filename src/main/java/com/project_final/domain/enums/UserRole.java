package com.project_final.domain.enums;

public enum UserRole {
    ADMIN, USER
}
