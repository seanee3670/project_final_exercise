package com.project_final.domain.dto.user;

import com.project_final.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserJoinResponse {
    private String userName;
    private Long userId;

    public static UserJoinResponse of(User user) {
        return new UserJoinResponse(user.getUserName(), user.getId());
    }
}
