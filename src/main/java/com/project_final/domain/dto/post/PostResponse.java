package com.project_final.domain.dto.post;

import com.project_final.domain.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostResponse {
    private String body;
    private Long postId;

    public static PostResponse of(Post post) {
        return new PostResponse(post.getBody(), post.getId());
    }
}
