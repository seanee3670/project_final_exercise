package com.project_final.domain.dto.comment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Access;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentRequest {
    private String comment;
}
