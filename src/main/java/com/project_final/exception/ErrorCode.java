package com.project_final.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ErrorCode {
    // USERNAME RELATED
    DUPLICATED_USER_NAME(HttpStatus.CONFLICT, "User with same UserName already exists."),
    USERNAME_NOT_FOUND(HttpStatus.NOT_FOUND, "User with the UserName does not exist."),

    // SECURITY RELATED
    INVALID_PASSWORD(HttpStatus.UNAUTHORIZED, "Password is incorrect."),
    INVALID_TOKEN(HttpStatus.UNAUTHORIZED, "Token is invalid"),
    NOT_EXIST_TOKEN(HttpStatus.UNAUTHORIZED,"Token does not exist."),
    INVALID_PERMISSION(HttpStatus.UNAUTHORIZED, "User has no permission."),

    // POST RELATED
    POST_NOT_FOUND(HttpStatus.NOT_FOUND, "Post cannot be found."),

    // COMMENT RELATED
    COMMENT_NOT_FOUND(HttpStatus.NOT_FOUND, "Comment cannot be found"),

    // LIKES RELATED
    DUPLICATED_LIKES(HttpStatus.CONFLICT, "Post has already been liked."),
    LIKES_NOT_FOUND(HttpStatus.NOT_FOUND, "Post has not been liked."),

    // DB RELATED
    DATABASE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Database has occurred an error."),

    // ETC
    UNKNOWN_ERROR(HttpStatus.BAD_REQUEST, "An unknown error has occurred.");
    
    private final HttpStatus httpStatus;
    private final String message;
}
