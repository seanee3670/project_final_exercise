package com.project_final.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice // 여러 컨트롤러에 대해 전역적으로 ExceptionHandler 적용
public class GlobalExceptionManager {

    @ExceptionHandler(AppException.class)
    public ResponseEntity<?> handleAppException(AppException e) {
        ErrorResponse error = new ErrorResponse(e.getMessage(), e.getErrorCode(), LocalDateTime.now());

        return ResponseEntity.status(e.getErrorCode().getHttpStatus())
                .body(error);
    }
}
