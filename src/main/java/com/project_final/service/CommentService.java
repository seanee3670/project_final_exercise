package com.project_final.service;

import com.project_final.domain.dto.comment.CommentDto;
import com.project_final.domain.dto.comment.CommentRequest;
import com.project_final.domain.dto.comment.CommentResponse;
import com.project_final.domain.entity.Comment;
import com.project_final.domain.entity.Post;
import com.project_final.domain.entity.User;
import com.project_final.exception.AppException;
import com.project_final.exception.ErrorCode;
import com.project_final.repository.CommentRepository;
import com.project_final.repository.PostRepository;
import com.project_final.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;
    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public CommentResponse addComment(String userName, Long postId, CommentRequest commentRequest) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        Post post = postRepository.findById(postId)
                .orElseThrow(()-> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        Comment comment = commentRepository.save(Comment.of(commentRequest.getComment(),post, user));

        return CommentResponse.of(comment);
    }

    public CommentResponse getComment(Long postId, Long commentId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(()-> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(()-> new AppException(ErrorCode.COMMENT_NOT_FOUND, ErrorCode.COMMENT_NOT_FOUND.getMessage()));

        return CommentResponse.of(comment);
    }

    public Page<CommentDto> getCommentList(Pageable pageable, Long postId) {
        Page<Comment> commentPage = commentRepository.findCommentsByPost(postId, pageable);
        Page<CommentDto> commentDtoPage = commentPage.map(comment -> CommentDto.toCommentDto(comment));

        return commentDtoPage;
    }

    public CommentResponse editComment(String userName, Long postId, Long commentId, CommentRequest commentRequest) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        Post post = postRepository.findById(postId)
                .orElseThrow(()-> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(()-> new AppException(ErrorCode.COMMENT_NOT_FOUND, ErrorCode.COMMENT_NOT_FOUND.getMessage()));
        if (!user.getId().equals(comment.getUser().getId())) {
            throw new AppException(ErrorCode.INVALID_PERMISSION, ErrorCode.INVALID_PERMISSION.getMessage());
        }

        comment.setComment(commentRequest.getComment());
        Comment editedComment = commentRepository.save(comment);
        return CommentResponse.of(editedComment);
    }

    public Long deleteComment(String userName, Long postId, Long commentId) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        Post post = postRepository.findById(postId)
                .orElseThrow(()-> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(()-> new AppException(ErrorCode.COMMENT_NOT_FOUND, ErrorCode.COMMENT_NOT_FOUND.getMessage()));
        if (!user.getId().equals(comment.getUser().getId())) {
            throw new AppException(ErrorCode.INVALID_PERMISSION, ErrorCode.INVALID_PERMISSION.getMessage());
        }
        commentRepository.deleteById(commentId);
        return commentId;
    }

}
