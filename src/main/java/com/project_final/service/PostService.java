package com.project_final.service;

import com.project_final.domain.dto.post.PostDto;
import com.project_final.domain.dto.post.PostRequest;
import com.project_final.domain.dto.post.PostResponse;
import com.project_final.domain.entity.Post;
import com.project_final.domain.entity.User;
import com.project_final.exception.AppException;
import com.project_final.exception.ErrorCode;
import com.project_final.repository.CommentRepository;
import com.project_final.repository.LikesRepository;
import com.project_final.repository.PostRepository;
import com.project_final.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class PostService {
    private final PostRepository postRepository;
    private final UserRepository userRepository;


    public PostResponse addPost(String userName, PostRequest postRequest) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        Post savedPost = postRepository.save(Post.of(postRequest.getTitle(), postRequest.getBody(), user));
        return PostResponse.of(savedPost);
    }

    public PostResponse getPost(Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(()-> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        return PostResponse.of(post);
    }


    public Page<PostDto> getPostList(Pageable pageable) {
        Page<Post> postPage = postRepository.findAll(pageable);
        Page<PostDto> postDtoPage = postPage.map(post -> PostDto.toPostDto(post));
        return postDtoPage;
    }

    public Page<PostDto> getMyFeed(Pageable pageable, String userName) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        Page<Post> postPage = postRepository.findPostsByUser(user, pageable);
        Page<PostDto> postDtoPage = postPage.map(post -> PostDto.toPostDto(post));
        return postDtoPage;
    }

    public PostResponse editPost(String userName, Long postId, PostRequest postRequest) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        Post post = postRepository.findById(postId)
                .orElseThrow(()-> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        if (!user.getId().equals(post.getUser().getId())) {
            throw new AppException(ErrorCode.INVALID_PERMISSION, ErrorCode.INVALID_PERMISSION.getMessage());
        }

        post.setTitle(postRequest.getTitle());
        post.setBody(postRequest.getBody());
        Post editedPost = postRepository.save(post);
        return PostResponse.of(editedPost);
    }

    public Long deletePost(String userName, Long postId) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        if (!user.getId().equals(post.getUser().getId())) {
            throw new AppException(ErrorCode.INVALID_PERMISSION, ErrorCode.INVALID_PERMISSION.getMessage());
        }

        postRepository.deleteById(postId);
        return postId;
    }






}
