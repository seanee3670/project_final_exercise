package com.project_final.service;

import com.project_final.domain.dto.user.UserJoinRequest;
import com.project_final.domain.dto.user.UserJoinResponse;
import com.project_final.domain.dto.user.UserLoginRequest;
import com.project_final.domain.dto.user.UserLoginResponse;
import com.project_final.domain.entity.User;
import com.project_final.exception.AppException;
import com.project_final.exception.ErrorCode;
import com.project_final.repository.UserRepository;
import com.project_final.utils.JwtUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.secret")
    private String secretKey;

    private final long expireTimeMs = 1000 * 60 * 60;

    public UserJoinResponse join(UserJoinRequest req) {
        // UserName 이미 존재시 -> DUPLICATED_USER_NAME 에러 처리
        userRepository.findByUserName(req.getUserName())
                .ifPresent(user -> {
                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME, ErrorCode.DUPLICATED_USER_NAME.getMessage());
                });

        // 보안처리된 비밀번호 -> userRepository 에 저장 -> return Response(저장된 유저)
        String encoded = encoder.encode(req.getPassword());
        User savedUser = userRepository.save(req.toEntity(encoded));

        return UserJoinResponse.of(savedUser);
    }

    public UserLoginResponse login(UserLoginRequest req) {
        User user = userRepository.findByUserName(req.getUserName()).orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        if (!encoder.matches(req.getPassword(), user.getPassword())) {
            throw new AppException(ErrorCode.INVALID_PASSWORD, ErrorCode.INVALID_PASSWORD.getMessage());
        }

        String token = JwtUtil.createToken(user.getUserName(), secretKey, expireTimeMs);
        return new UserLoginResponse(token);
    }
}
