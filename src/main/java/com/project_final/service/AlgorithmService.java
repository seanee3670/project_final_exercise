package com.project_final.service;

public class AlgorithmService {
    public static int sumOfDigits(int digits) {
        int sum = 0;
        while (digits != 0) {
            sum += digits % 10;
            digits /= 10;
        }

        return sum;
    }
}
