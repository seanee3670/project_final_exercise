package com.project_final.service;

import com.project_final.domain.entity.Likes;
import com.project_final.domain.entity.Post;
import com.project_final.domain.entity.User;
import com.project_final.exception.AppException;
import com.project_final.exception.ErrorCode;
import com.project_final.repository.LikesRepository;
import com.project_final.repository.PostRepository;
import com.project_final.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LikesService {
    private final LikesRepository likesRepository;
    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public long likePost(String userName, Long postId) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        Post post = postRepository.findById(postId)
                .orElseThrow(()-> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        likesRepository.findByUserAndPost(user, post)
                .ifPresent(likes -> {
                    throw new AppException(ErrorCode.DUPLICATED_LIKES, ErrorCode.DUPLICATED_LIKES.getMessage());
                });
        Likes likes = likesRepository.save(Likes.of(user, post));

        return likes.getId();
    }

    public Long unlikePost(String userName, Long postId) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        Post post = postRepository.findById(postId)
                .orElseThrow(()-> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        Likes likes = likesRepository.findByUserAndPost(user, post)
                .orElseThrow(()-> new AppException(ErrorCode.LIKES_NOT_FOUND, ErrorCode.LIKES_NOT_FOUND.getMessage()));
        likesRepository.delete(likes);

        return postId;
    }


    public Long countLikes(Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(()-> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        Long likes = likesRepository.countByPost(post);

        return likes;
    }

}
