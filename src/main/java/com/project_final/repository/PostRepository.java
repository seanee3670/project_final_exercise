package com.project_final.repository;

import com.project_final.domain.entity.Post;
import com.project_final.domain.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {
    Page<Post> findPostsByUser(User user, Pageable pageable);
}
