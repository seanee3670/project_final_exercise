package com.project_final.repository;

import com.project_final.domain.entity.Comment;
import com.project_final.domain.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    Page<Comment> findCommentsByPost(Long postId, Pageable pageable);

    void deleteAllByPost(Post post);
}
