package com.project_final.repository;

import com.project_final.domain.entity.Likes;
import com.project_final.domain.entity.Post;
import com.project_final.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LikesRepository extends JpaRepository<Likes, Integer> {
    Optional<Likes> findByUserAndPost(User user, Post post);

    Long countByPost(Post post);

    void deleteAllByPost(Post post);
}
