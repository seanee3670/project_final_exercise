package com.project_final.controller;

import com.project_final.domain.dto.Response;
import com.project_final.service.LikesService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/v1/posts")
public class LikesController {
    private final LikesService likeService;

    @PostMapping("{postId}/likes")
    public ResponseEntity<Response> likePost(Authentication authentication, @Parameter(description = "post id") @PathVariable Long postId) {
        likeService.likePost(authentication.getName(), postId);
        return ResponseEntity.ok().body(Response.success("post has been liked"));
    }

    @DeleteMapping("{postId}/likes")
    public ResponseEntity<Response> unlikePost(Authentication authentication, @Parameter(description = "post id") @PathVariable Long postId) {
        likeService.unlikePost(authentication.getName(), postId);
        return ResponseEntity.ok().body(Response.success("post has been unliked"));
    }

    @GetMapping("{postId}/likes")
    public ResponseEntity<Response> countLikes(@Parameter(description = "post id") @PathVariable Long postId) {
        Long likes = likeService.countLikes(postId);
        return ResponseEntity.ok().body(Response.success(likes));
    }
}
