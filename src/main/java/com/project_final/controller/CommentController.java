package com.project_final.controller;

import com.project_final.domain.dto.Response;
import com.project_final.domain.dto.comment.CommentDeleteResponse;
import com.project_final.domain.dto.comment.CommentDto;
import com.project_final.domain.dto.comment.CommentRequest;
import com.project_final.domain.dto.comment.CommentResponse;
import com.project_final.service.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/posts")
public class CommentController {
    private final CommentService commentService;

    @PostMapping("/{postId}/comments")
    public ResponseEntity<Response> addComment(Authentication authentication, @PathVariable Long postId, @RequestBody CommentRequest commentRequest) {
        CommentResponse commentResponse = commentService.addComment(authentication.getName(), postId, commentRequest);
        return ResponseEntity.ok().body(Response.success(commentResponse));
    }

    @GetMapping("/{postId}/comments")
    public ResponseEntity<Response> getCommentList(@PageableDefault(size = 20) Pageable pageable, @PathVariable Long postId) {
        Page<CommentDto> CommentDtoPage = commentService.getCommentList(pageable, postId);
        return ResponseEntity.ok().body(Response.success(CommentDtoPage));
    }

    @GetMapping("/{postId}/comments/{commentId}")
    public ResponseEntity<Response> getComment(@PathVariable Long postId, @PathVariable Long commentId) {
        CommentResponse commentResponse = commentService.getComment(postId, commentId);
        return ResponseEntity.ok().body(Response.success(commentResponse));
    }

    @PutMapping("/{postId}/comments/{commentId}")
    public ResponseEntity<Response> editComment(Authentication authentication, @PathVariable Long postId, @PathVariable Long commentId, @RequestBody CommentRequest commentRequest) {
        CommentResponse commentResponse = commentService.editComment(authentication.getName(), postId, commentId, commentRequest);
        return ResponseEntity.ok().body(Response.success(commentResponse));
    }

    @DeleteMapping("{postId}/comments/{commentId}")
    public ResponseEntity<Response> deleteComment(Authentication authentication, @PathVariable Long postId, @PathVariable Long commentId) {
        Long deletedCommentId = commentService.deleteComment(authentication.getName(), postId, commentId);
        return ResponseEntity.ok().body(Response.success(new CommentDeleteResponse("comment deleted", deletedCommentId)));
    }

}
