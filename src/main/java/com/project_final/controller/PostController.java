package com.project_final.controller;

import com.project_final.domain.dto.Response;
import com.project_final.domain.dto.post.PostDto;
import com.project_final.domain.dto.post.PostRequest;
import com.project_final.domain.dto.post.PostResponse;
import com.project_final.service.PostService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/v1/posts")
public class PostController {
    private final PostService postService;

    @PostMapping
    public ResponseEntity<Response> addPost(Authentication authentication, @RequestBody PostRequest postRequest) {
        String userName = authentication.getName();
        log.info("userName = {}", userName);

        PostResponse postResponse = postService.addPost(userName, postRequest);
        return ResponseEntity.ok().body(Response.success(postResponse));
    }

    @GetMapping("/{postId}")
    public ResponseEntity<Response> getPost(@PathVariable Long postId) {
        log.info("postId = {}", postId);

        PostResponse postResponse = postService.getPost(postId);
        return ResponseEntity.ok().body(Response.success(postResponse));
    }

    @GetMapping
    public ResponseEntity<Response> getPostList(Pageable pageable) {
        Page<PostDto> postDtoPage = postService.getPostList(pageable);
        return ResponseEntity.ok().body(Response.success(postDtoPage));
    }

    @GetMapping("/my")
    public ResponseEntity<Response> getMyFeed(Authentication authentication, Pageable pageable) {
        Page<PostDto> postDtoPage = postService.getMyFeed(pageable, authentication.getName());
        return ResponseEntity.ok().body(Response.success(postDtoPage));
    }

    @PutMapping("/{postId}")
    public ResponseEntity<Response> editPost(Authentication authentication, @PathVariable Long postId, @RequestBody PostRequest postRequest) {
        String userName = authentication.getName();
        log.info("postId = {}", postId);

        PostResponse postResponse = postService.editPost(userName, postId, postRequest);
        return ResponseEntity.ok().body(Response.success(postResponse));
    }

    @DeleteMapping("/{postId}")
    public ResponseEntity<Response> deletePost(Authentication authentication, @PathVariable Long postId) {
        String userName = authentication.getName();
        log.info("postId = {}", postId);

        Long deletedPostId = postService.deletePost(userName, postId);
        return ResponseEntity.ok().body(Response.success(new PostResponse("post deleted", deletedPostId)));
    }

}
